/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author informatics
 */
public class Lab2 {
    static String[][] table = {
        {"1","2","3"},
        {"4","5","6"},
        {"7","8","9"}
    };
    static String currentPlayer = "X";
    static String num;
    static Set<String> chosenNumbers = new HashSet<>();
    static boolean gameOver = false;

    static void printWelcome(){
       System.out.println("Welcome to XO");
    }
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    static void printTurn(){
        System.out.println(currentPlayer+ " Turn");
    }
    static void inputNumber(){
        Scanner sc = new Scanner(System.in);
            System.out.println("Please input number :");
            num = sc.next();
            checkRepeat();
            changeTable( num);
    }
    static void changeTable( String num){
            if(num.equals("1")){
                table[0][0] = currentPlayer;
            } else if (num.equals("2")) {
                table[0][1] = currentPlayer;
            } else if (num.equals("3")) {
                table[0][2] = currentPlayer;
            } else if (num.equals("4")) {
                table[1][0] = currentPlayer;
            } else if (num.equals("5")) {
                table[1][1] = currentPlayer;
            } else if (num.equals("6")) {
                table[1][2] = currentPlayer;
            } else if (num.equals("7")) {
                table[2][0] = currentPlayer;
            } else if (num.equals("8")) {
                table[2][1] = currentPlayer;
            } else if (num.equals("9")) {
                table[2][2] = currentPlayer;
            }
    }
    static void switchPlayer(){
        if(currentPlayer=="X"){
            currentPlayer = "O";
        }else{
            currentPlayer = "X";
        }
    }
    static boolean isWin(){
        if((checkrow() || checkcol() || checkDiagonal())){
            return true;
        }else{
            return false;
        }
    } 
    static boolean isDraw(){
        if (table[0][0] == "1" || table[0][1] == "2"  || table[0][2] == "3"  || table[1][0] == "4"  || table[1][1] == "5"  || table[1][2] == "6"  || table[2][0] == "7"  || table[2][1] == "8"  || table[2][2] == "9" ) {
            return false;
        }
        return true;
    }
    static boolean checkrow(){
        for(int i=0;i<3;i++){
            if(table[0][0]== currentPlayer && table[0][1]== currentPlayer&&table[0][2]== currentPlayer){
                return true;
            }else if(table[1][0]== currentPlayer && table[1][1]== currentPlayer&&table[1][2]== currentPlayer){
                return true;
            }else if(table[2][0]== currentPlayer && table[2][1]== currentPlayer&&table[2][2]== currentPlayer){
                return true;
            }
        }
        return false;
    }
    static boolean checkcol(){
        for(int i=0;i<3;i++){
            if(table[0][0]== currentPlayer && table[1][0]== currentPlayer&&table[2][0]== currentPlayer){
                return true;
            }else if(table[0][1]== currentPlayer && table[1][1]== currentPlayer&&table[2][1]== currentPlayer){
                return true;
            }else if(table[0][2]== currentPlayer && table[1][2]== currentPlayer&&table[2][2]== currentPlayer){
                return true;
            }
        }
        return false;
    }
    static boolean checkDiagonal(){
        for(int i=0;i<3;i++){
            if(table[0][0]== currentPlayer && table[1][1]== currentPlayer&&table[2][2]== currentPlayer){
                return true;
            }else if(table[0][2]== currentPlayer && table[1][1]== currentPlayer&&table[2][0]== currentPlayer){
                return true;
            }            
        }
        return false;
    }
    static void printWin(){
        System.out.println(currentPlayer + " Win!!!");
    }
    static void printDraw(){
        System.out.println(" Draw!!!");
    }
    static void newGame(){
        System.out.println("Play agian (Y/N) :");
        Scanner sc = new Scanner(System.in);
        String playAgain = sc.next();
        if(playAgain.equals("Y")){
            resetGame();
        }
    }
    static void checkRepeat(){
        while (chosenNumbers.contains(num)) {
            printTable();
            printTurn();
            inputNumber();
            }
        chosenNumbers.add(num);
        
    }
    static void resetGame(){
        chosenNumbers.clear();
        currentPlayer = "X";
        table = new String[][] {
            {"1","2","3"},
            {"4","5","6"},
            {"7","8","9"}
        };
    }
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printTable();
            printTurn();
            inputNumber();
            if(isWin()){
                printTable();
                printWin();
                gameOver = true;
                if(gameOver == true){
                    newGame();
                    continue;
                }
                break;
            }
            if(isDraw()){
                printDraw();
                gameOver = true;
                if(gameOver == true){
                    newGame();
                    continue;
                }
                break;

            }
                
            switchPlayer();
        }
    }
}    

